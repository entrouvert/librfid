# -*- coding: utf-8 -*-
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Functions to convert human readable to frame encoded data at command
    building. Conversely at response parsing.

    The same processing function (command name lowered) is used for building
    the command and for parsing the response. It is necessary to test the
    parameter object to affect the correspondiong treatment.
'''


from librfid.exceptions import MalformedDataError


def inventory(response):
    '''
        Parse response content of 'Inventory' command.

        Return a list of dict describing tags.
    '''
    tags = list()
    if not response.raw_data:
        return tags
    nb_tags = int(response.raw_data[0].encode('hex'), 16)
    end = len(response.raw_data)
    index = 1
    for _ in range(0, nb_tags):
        tag = dict()
        if index + 5 <= end:
            tag['EPCLen'] = int(response.raw_data[index].encode('hex'), 16)
            index += 1
        else :
            raise MalformedDataError('Data content is malformed.')
        if index  + 3 + tag['EPCLen'] <= end:
            tag['EPC'] = response.raw_data[index:index+tag['EPCLen']]
            index += tag['EPCLen']
            tag['AntID'] = int(response.raw_data[index].encode('hex'), 16)
            index += 1
            tag['NbRead'] = \
                int(response.raw_data[index:index+2].encode('hex'), 16)
            index += 2
        else:
            raise MalformedDataError('Data content is malformed.')
        tags.append(tag)
    response.data = tags
