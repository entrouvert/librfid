# -*- coding: utf-8 -*-
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Helper functions
'''


import crc16
from binascii import unhexlify

from librfid.exceptions import OverheadException


def get_length_on_2_bytes(string):
    length = len(string)
    fmt = '%%0%dx' % (16 // 4)
    return unhexlify(fmt % length)

def get_integer_on_two_bytes(integer):
    if integer > 65535:
        raise OverheadException('Integer to big to be converted in a string '
            'of two bytes')
    fmt = '%%0%dx' % (16 // 4)
    return unhexlify(fmt % integer)

def get_crc_16_CCITT(string):
    # Polynome x16 + x12 + x5 + 1, 0x1021
    crc = crc16.crc16xmodem(string, 0xffff)
    fmt = '%%0%dx' % (16 // 4)
    return unhexlify(fmt % crc)

def switch_big_to_little_indian(string):
    return string[::-1]

def hex_to_int(hex_value):
    return int(hex_value.encode('hex'), 16)
