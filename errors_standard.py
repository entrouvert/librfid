# -*- coding: utf-8 -*-
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    EPC1 GEN2 / ISO 18000-6C errors
'''


ERRORS = {
    '\x01': 'Autre erreur retournée par la puce différente de 03h, 04h et '
        '0Bh. Retenter l’opération.',
    '\x02': 'Mauvais paramètre de puce. Vérifier les paramètres de la '
        'commande liés à la puce.',
    '\x03': 'Adressage mémoire refusé. L’adresse mémoire saisie doit être '
        'hors de la mémoire disponible sur le tag.',
    '\x04': 'Mémoire verrouillée. L’accès est refusé car la mémoire est '
        'verrouillée. Changer de zone ou déverrouiller la mémoire.',
    '\x07': 'Erreur RF. Pas de tag détecté ou le masque de détection est '
        'trop restrictif. Approcher le tag ou modifier le masque.',
    '\x08': 'Erreur RF lors d’une commande Lock ou mauvaise valeur de mot de '
        'passe pour tag verrouillé. Une opération de Lock sur un tag '
        'sécurisé (mot de passe différent de 0000) a été tentée avec un mot '
        'de passe égal à 0000. Changer ce mot de passe.',
    '\x0b': 'Puissance insuffisante. La puissance actuelle du lecteur ne '
        'permet pas de finir l’opération en cours. Augmenter la puissance '
        'ou rapprocher le tag.',
    '\x0f': 'Mauvais mot de passe. Le mot de passe saisit ne permet pas '
        'l’accès.',
    '\x11': 'Erreur identique 01h lors d’une vérification après commande '
        'Write. Erreur de vérification.',
    '\x14': 'Erreur identique 04h lors d’une vérification après commande '
        'Write. Erreur lors de la vérification de l’opération en cours sur '
        'la mémoire verrouillée.',
    '\x17': 'Erreur identique 07h lors d’une vérification après commande '
        'Write. Erreur de vérification de l’opération RF en cours. Approcher '
        'le tag ou modifier le masque.',
    '\x1b': 'Erreur identique 0Bh lors d’une vérification après commande '
        'Write. Erreur lors de la vérification de l’opération en cours à '
        'cause d’une puissance insuffisante.',
}
