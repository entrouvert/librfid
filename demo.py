#!/usr/bin/env python
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Demo
'''

# Run with python -m librfid.demo


import librfid

def inventory(reader):
    print 'Inventory'
    try:
        command = librfid.Command(command_name='Inventory')
        response = reader.send(command)
        command.pretty_print()
        response.pretty_print()
        for tag in response.data:
            print "Tag: %s" % tag['EPC'].encode('hex')
            print "\t Read on logic port: %s" % tag['AntID']
            print "\t Times read on all logic ports: %s" % tag['NbRead']
    except Exception, err:
        print str(err)

def get_rf_settings(reader):
    print 'Get_RFSettings'
    try:
        command = librfid.ReaderCommand(command_name='Get_RFSettings',
            reader=reader)
        response = reader.send(command)
        command.pretty_print()
        response.pretty_print()
        i = 0
        for logic_port in response.data:
            if logic_port['ScanDelay'] or logic_port['PowerAnt']:
                print "Logic port: %s" % i
                print "\tScanDelay: %s ms" % logic_port['ScanDelay']
                print "\tPowerAnt: %s dBm" % logic_port['PowerAnt']
                print "\tAntNb: %s" % logic_port['AntNb']
                i += 1
    except Exception, err:
        print str(err)

def set_rf_settings(reader):
    print 'Set_RFSettings'
    try:
        logic_port  = {\
            'ScanDelay': 800,
            'PowerAnt': 23,
            'AntNb': 0,
        }
        logic_ports = [logic_port]
        command = librfid.ReaderCommand(command_name='Set_RFSettings',
            reader=reader, data=logic_ports)
        response = reader.send(command)
        command.pretty_print()
        response.pretty_print()
    except Exception, err:
        print str(err)

def set_rf_settings_saved(reader):
    print 'Set_RFSettings_Saved'
    try:
        logic_port  = {\
            'ScanDelay': 800,
            'PowerAnt': 23,
            'AntNb': 0,
        }
        logic_ports = [logic_port]
        command = librfid.ReaderCommand(command_name='Set_RFSettings_Saved',
            reader=reader, data=logic_ports)
        response = reader.send(command)
        command.pretty_print()
        response.pretty_print()
    except Exception, err:
        print str(err)

def reset_rf_settings(reader):
    print 'Reset_RFSettings'
    try:
        command = librfid.ReaderCommand(command_name='Reset_RFSettings',
            reader=reader)
        response = reader.send(command)
        command.pretty_print()
        response.pretty_print()
    except Exception, err:
        print str(err)

def get_health_parameters(reader):
    print 'Get_HealthParameters'
    quality = ['Disconnected', 'Bad', 'Good']
    try:
        command = librfid.ReaderCommand(command_name='Get_HealthParameters',
            reader=reader)
        response = reader.send(command)
        command.pretty_print()
        response.pretty_print()
        health_parameters = response.data
        for i in range(4):
            tune = "Tune{}".format(i)
            print "Antenna {} connection quality : {} ({})".format(i,
                quality[health_parameters[tune][1]], health_parameters[tune][0])
        print "Reader core temperature: %d" % health_parameters['TempCore']
        print "Reader power amplificator temperature: %d" % health_parameters['PA']
    except Exception, err:
        print str(err)

def test_1(reader):
    get_rf_settings(reader)
    inventory(reader)
    set_rf_settings(reader)
    get_rf_settings(reader)
    inventory(reader)

def test_2(reader):
    inventory(reader)
    set_rf_settings_saved(reader)
    get_rf_settings(reader)
    inventory(reader)

def test_3(reader):
    get_rf_settings(reader)
    inventory(reader)

def test_4(reader):
    test_3(reader)
    reset_rf_settings(reader)

def test_5(reader):
    get_health_parameters(reader)

if __name__ == '__main__':
    reader = librfid.RFIDReader()
    # Detect same tags before and after changing RF parameters
#    test_1(reader)

    # Detect same tags before and after changing RF parameters and reader
    # reboot
#    test_2(reader)
    # Do hard reader reboot
#    test_3(reader)

    # Test reset : Reset_RFSettings besoin d'un reboot
#    test_4(reader)
    # Do hard reader reboot
#    test_3(reader)

    get_health_parameters(reader)
    get_rf_settings(reader)
    inventory(reader)
    try:
        logic_port  = {
            'ScanDelay': 280,
            'PowerAnt': 31,
            'AntNb': 0,
        }
        logic_ports = [logic_port]
        command = librfid.ReaderCommand(command_name='Set_RFSettings',
            reader=reader, data=logic_ports)
        response = reader.send(command)
        command.pretty_print()
        response.pretty_print()
    except Exception, err:
        print str(err)
    get_rf_settings(reader)
    inventory(reader)
