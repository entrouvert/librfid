# -*- coding: utf-8 -*-
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    STID reader specific commands
'''


COMMANDS = {
    'Set_RFSettings': {
            'type': '\x00',
            'code': '\x00\x21',
        },
    'Reset_RFSettings': {
            'type': '\x00',
            'code': '\x00\x23',
            'description': 'Cette commande permet de réinitialiser les '
        'paramètres par défauts des cycles de lecture effectués lors des '
        'commandes Inventory. Ces informations seront sauvegardées en '
        'EEProm. Il est nécessaire de redémarrer le lecteur afin que cette '
        'commande soit prise en compte.',
        },
    'Set_RFSettings_Saved': {
            'type': '\x00',
            'code': '\x00\x22',
        },
    'Get_RFSettings': {
            'type': '\x00',
            'code': '\x00\x20',
        },
    'Get_HealthParameters': {
            'type': '\x00',
            'code': '\x00\x24',
        },
    'Autonomous_Start': {
            'type': '\x00',
            'code': '\x00\x10',
        },
    'Autonomous_Stop': {
            'type': '\x00',
            'code': '\x00\x11',
        },
    'Autonomous_Output': {
            'type': '\x00',
            'code': '\x00\x12',
        },
    'SetOptoOutputParam': {
            'type': '\x00',
            'code': '\x00\x25',
        },
    'ChangeRegulation': {
            'type': '\x00',
            'code': '\x00\x26',
        },
    'GetInfos': {
            'type': '\x00',
            'code': '\x00\x08',
        },
    'SetBaudRate': {
            'type': '\x00',
            'code': '\x00\x05',
        },
    'Set485Address': {
            'type': '\x00',
            'code': '\x00\x06',
        },
    'Set_RF_Param': {
            'type': '\x00',
            'code': '\x00\x27',
        },
    'Retrieve_RF_Params': {
            'type': '\x00',
            'code': '\x00\x28',
        },
}
