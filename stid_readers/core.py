# -*- coding: utf-8 -*-
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    Functions to convert human readable to frame encoded data at command
    building. Conversely at response parsing.

    The same processing function (command name lowered) is used for building
    the command and for parsing the response. It is necessary to test the
    parameter object to affect the correspondiong treatment.
'''


from librfid.exceptions import MalformedDataError, BadParameterException
from librfid.utils import get_integer_on_two_bytes, hex_to_int


def get_rfsettings(response):
    '''
        Parse response content of 'Get_RFSettings' command.

        Return a list of dict describing logic ports of the reader.
    '''
    if not response.raw_data:
        raise MalformedDataError('Data content is malformed.')
    if len(response.raw_data) != 64:
        raise MalformedDataError('Data content is malformed.')
    logic_ports = list()
    for i in range(0, 16):
        logic_port = dict()
        # 0 ms ≤ ScanDelay ≤ 655350 ms ()
        logic_port['ScanDelay'] = \
            hex_to_int(response.raw_data[i * 4:(i * 4) + 2]) * 10
        power_ant_nb = hex_to_int(response.raw_data[(i * 4) + 2:(i * 4) + 4])
        logic_port['AntNb'] = power_ant_nb & 0x000f
        logic_port['PowerAnt'] = (power_ant_nb >> 4) / 10
            #[0E6h ≤ PowerAnt ≤ 136h] soit [23 dBm ≤ PowerAnt ≤ 31 dBm] (URF)
        logic_ports.append(logic_port)
    response.data = logic_ports


def set_rfsettings(command):
    '''
        Parse response content of 'Get_RFSettings' command.

        Return a string representing the data from a list of dict describing
        logic ports of the reader..
    '''
    if not command.data:
        raise MalformedDataError('Data content is malformed.')
    logic_ports = command.data
    data = ''
    i = 0
    for logic_port in logic_ports:
        if logic_port['ScanDelay'] > 655350 or logic_port['PowerAnt'] > 31:
            raise BadParameterException('One bad parameter for logic port '
                '%s: ScanDelay %s PowerAnt %s'
                % (i, logic_port['ScanDelay'], logic_port['PowerAnt']))
        data += get_integer_on_two_bytes(logic_port['ScanDelay'] / 10)
        power_ant_nb = (logic_port['PowerAnt'] * 10) << 4
        power_ant_nb |= logic_port['AntNb']
        data += get_integer_on_two_bytes(power_ant_nb)
        i += 1
    for _ in range(i, 16):
        data += '\x00\x00\x00\x00'
    command.raw_data = data


def set_rfsettings_saved(command):
    return set_rfsettings(command)


def get_healthparameters(response):
    '''
        Parse response content of 'Get_HealthParameters' command.

        Return a dict of values.
    '''
    def antenna_quality(value):
        if value <= 100:
            return 0
        elif value > 160:
            return 2
        else:
            return 1
    if not response.raw_data:
        raise MalformedDataError('Data content is malformed.')
    if len(response.raw_data) != 6:
        raise MalformedDataError('Data content is malformed.')
    health_parameters = {
        'Tune{}'.format(i): (hex_to_int(response.raw_data[i]),
            antenna_quality(hex_to_int(response.raw_data[i])))
                for i in range(4)
    }
    health_parameters['TempCore'] = hex_to_int(response.raw_data[4])
    health_parameters['PA'] = hex_to_int(response.raw_data[5])
    response.data = health_parameters
