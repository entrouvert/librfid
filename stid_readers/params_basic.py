# -*- coding: utf-8 -*-
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    STID reader specific errors
'''


from serial import (EIGHTBITS, PARITY_NONE, STOPBITS_ONE)


PARAMS = {
        'reader_type': 'SERIAL',
        'port': '/dev/ttyUSB0',
        'baudrate': 115200,
        'bytesize': EIGHTBITS,
        'parity': PARITY_NONE,
        'stopbits': STOPBITS_ONE,
        'timeout': 5,
        'xonxoff': False,
        'rtscts': False,
        'writeTimeout': None,
        'dsrdtr': False,
        'interCharTimeout': None,
}
