# -*- coding: utf-8 -*-
'''
    librfid - RFID EPC Class 1 Gen2 / ISO/IEC 18000-6C compliant library

    Copyright (C) 2013  Entr'ouvert

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


    STID reader specific errors
'''


ERRORS = {
    '\x02': 'Mauvais paramètre de donnée. Mauvais paramètre ou mauvais '
        'déchiffrement. Vérifier les paramètres.',
    '\x03': 'Erreur de CRC sur la trame Problème de bruit sur la liaison. '
        'Mauvaise communication série.',
    '\x04': 'Mauvaise longueur de trame reçue Problème de bruit sur la '
        'liaison. Mauvaise communication série.',
    '\x07': 'Mauvais code de commande. Vérifier la commande.',
    '\x08': 'Mauvais type de commande. Vérifier le type de commande.',
    '\x20': 'Problème hardware du lecteur UHF Problème lié à la qualité ou '
        'la connexion d’antenne ou température interne du lecteur (cœur ou '
        'amplificateur de puissance) trop élevée.',
    '\xd1': 'Problème non récurrent.',
    '\xd2': 'Problème non récurrent.',
    '\xd3': 'Problème matériel. Analyse nécessaire.',
}
